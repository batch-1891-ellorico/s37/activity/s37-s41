// Require 'model' as if it is a model or package
const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName, 
		lastName: reqBody.lastName,
		email: reqBody.email, 
		mobileNo: reqBody.mobileNo, 
		password: bcrypt.hashSync(reqBody.password, 10) 
		//May dalawang parameter: hashSync(<dataToBeHash>, <saltRound>)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

// User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			// Compare Sync compares passwords. it has two parameters compareSync(<dataToCompare>, <encryptedPassword>)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			// will return either true or false. Depending on the result of the comparison. 

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

// Retrieve User Details
module.exports.getProfile = (userData) => {
	return User.findOne({id : userData.id}).then(result => {
		if (result == null){
			return false
		} else {
			result.password = ""
			return result
		}
	});
};

// Enroll User to A Class
// Async kapag may data na kailangan iprocess
// Kailangan gumamit ng async and await para bago magsend ng response kay client, hihintayin niya muna magprocess ung is user saka is course. Kapag naprocess na niya yung result ng dalawa, saka pa lang siya magtatapon ng response sa client. 
module.exports.enroll = async (data) => {
	if (data.isAdmin === true) {
		return 'Admin not allowed to Enroll'
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId})
		return user.save().then((user, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	})
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})
	if (isUserUpdated && isCourseUpdated) {
		return true
	} else {
		return false
	}
	}
}

