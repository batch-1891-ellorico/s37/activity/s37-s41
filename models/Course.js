// require mongoose para makapagcreate ng schema. 
const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema  ({

		name: {
			type: String, 
			// for validation si required
			required: [true, "Course is required"]
			}, 
		description: {
			type: String, 
			required: [true, "Description is required"]
			}, 
		isActive: {
			type: Boolean, 
			default: true
			}, 
		price: {
			type: Number, 
			required: [true, "Price is required"]
			}, 
		createdOn: {
			type: Date, 
			// New date function. Naglalagay lang tayo ng current date ang time kung kailan siya nastore sa atng database
			default: new Date()
			}, 
		enrollees: [
		// this will accept array of objects. 
			{
				userId: {
					type: String, 
					required: [true, "UserID is required."]
				}, 
				enrolledOn: {
					type: Date, 
					default: new Date()
				}
			}
		]
})


// Kailangan natin siyang iexport as a module. Kailangan itreat niya ito as a seperate package. 
module.exports = mongoose.model("Course", courseSchema);