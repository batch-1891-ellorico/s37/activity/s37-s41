const express = require('express');
// Para magamit si Mongoose, need irequire
const mongoose = require('mongoose')
// irequire si cors. Allows our backend application to be available to our front end application
const cors = require('cors')
// Require user router
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');
const port = 4000;

// Store express in an app variable to be used
const app = express ();

// Use the middlewares to be used in applications. Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


// Para maiconnect kay MongoDB
// Copied from MongoDB, connect. Also, change password <password>
// After ng / and before ?, ilagay ang folder name sa mongoDB
mongoose.connect("mongodb+srv://admin123:admin123@zuitt-bootcamp.cnhvgta.mongodb.net/S37-s41?retryWrites=true&w=majority", {
	// Dati, okay lang na wala sila. Kaya lang because of the updates of the Mongoose, kapag hindi naglalagay ng ganito, nagkakaroon siya ng error. 
	useNewUrlParser: true, 
	useUnifiedTopology: true
})

// Connect kay MongoDB
let db = mongoose.connection;

// I-bind kapag may error kapag hindi nakapagconnect sa mongoDB
// on and once parehas tumatanggap ng event. pero sa once, once connection is successful.
// on and once is both an event. 
db.on('error', () => console.error.bind(console, 'error'))
// Message na lalabas sa port kapag nakapagconnect neto sa database. 
db.once('open', () => console.log('Now connected to MongoDB Atlas'))


// To listen to a port
// Kapag dineploy na at gumagamit na ng hosted applicatio, ito yung buong code para if ever na hindi available yung isa, pwedeng magamit sa ibang local application
// Kapag dineploy na siya online, si Heroku na magdedecide kung papaano siya magrarun. Para lang flexible siya. Kaya may ganito: process.env.PORT || port. May OR sa gitna
app.listen(process.env.PORT || port, () => {
		// Message that this is running
	console.log(`API is now online on port ${process.env.PORT || port}`)
	})


