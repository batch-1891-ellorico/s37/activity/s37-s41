const express = require('express');
const router = express.Router(); 
const userController = require('../controllers/userController');
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for retrieving user details
// Bago magprocess yung request, dumadaan muna siya sa "auth.verify" that serves as a middleware. 
router.get("/details", auth.verify, (req, res) => {
// Provides the user's ID for the getProfile controller method

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	console.log(req.headers.authorization)

	userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController))
});

router.post("/enroll", auth.verify, (req, res) => {

	userIdData = auth.decode(req.headers.authorization)
	// console.log(userIdData.id)

	let data = {
		userId: userIdData.id,
		isAdmin: userIdData.isAdmin,
		courseId: req.body.courseId
	}
	// tatawagin si usrcontroller, gawa ng function enroll from data. 
	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router; 