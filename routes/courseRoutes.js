const express = require('express');
const router = express.Router();
const auth = require("../auth")
const courseController = require('../controllers/courseController');

// Route for creating a course
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === true) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
});

// Route for retrieving all our courses
router.get("/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
})

// Route for retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
}); 

// Route for retrieving a specific course
// localhost:4000/courses/312342234
router.get("/:courseId", (req, res) => {
	console.log(req.params)
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
}); 

//Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// Route for archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	courseController.archiveCourse(userData, req.params, req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router;